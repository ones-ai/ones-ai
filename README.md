# ONES AI

[![Pipeline Status](https://gitlab.com/ones-ai/ones-ai/badges/main/pipeline.svg?ignore_skipped=true)](https://gitlab.com/ones-ai/ones-ai/-/commits/main)
[![Release Status](https://gitlab.com/ones-ai/ones-ai/-/badges/release.svg)](https://gitlab.com/ones-ai/ones-ai/-/releases)
[![Documentation](https://img.shields.io/badge/documentation-Wiki-blue)](https://gitlab.com/ones-ai/ones-ai/-/wikis/home)

## Introduction

<!-- This is a brief introduction for this project. Architecture, screenshots, and maybe some gifs will help people understand. -->
ONES-AI is an open-source project led by ETRI. The ONES-AI compiler is a DNN compiler and an execution framework for neural processing units. It is designed to be used as a backend for high-level DNN frameworks and to allow state-of-the-art compiler optimizations.

## [Wiki](https://gitlab.com/ones-ai/ones-ai/-/wikis/home)

For more information, you can visit our [wiki](https://gitlab.com/ones-ai/ones-ai/-/wikis/home) for detailed documentation.

## Getting started

### Prerequisite

If there is any specific hardware or software requirement, it can be documented here.

### Install

Add the installation insturctions how to prepare the required environment including packages to be installed.

### Build

Add how to build this project.

### Test


### Release

Create a release with a tag to release this project. It requires at lease maintainer privilege to create a release.

## Support


## Roadmap

If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing

Your contributions are welcomed and encouraged! Create a merge request to submit your contributions.
You must pass our CI/CD pipeline for your merge requests to be accepted.

When you are committing a change, it must be signed off by your GPG keys. For more about signing commits with GPG, you may refer to the [GitLab's documentation](https://docs.gitlab.com/ee/user/project/repository/signed_commits/gpg.html). Please follow the guideline carefully and register your public key to gitlab.com so that we can verify your commits. Verification must pass at the `check` stage of the CI/CD pipeline.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup.

## Authors and acknowledgment

Show your appreciation to those who have contributed to the project.

## License

Dual License - Apache 2.0, Commercial License


### Contacts

Question: leejaymin@etri.re.kr
License: msyu@etri.re.kr
If you want to use and/or redistribute this source commercially, please consult
yongin.kwon@etri.re.kr for details in advance.

